# infosurrd

> Web estática para Yafá Mateo

# Documentación de componentes
https://vuetifyjs.com/en/components/api-explorer/

# Listado de iconos
https://cdn.materialdesignicons.com/5.1.45/

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
